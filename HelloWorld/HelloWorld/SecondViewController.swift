//
//  SecondViewController.swift
//  HelloWorld
//
//  Created by Jonathan Parra on 10/18/19.
//  Copyright © 2019 Jonathan Parra. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var myViewTitelLabel: UILabel!
    
    var customTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myViewTitelLabel.text = customTitle
    }
    
    @IBAction func changeButtonPressed(_ sender: Any) {
        myViewTitelLabel.text = "New Title"
    }
    
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
