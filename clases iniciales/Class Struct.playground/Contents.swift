import UIKit


// Classes & Struct

struct Car {
    var maxSpeed: Int
    var brand: String

//    NO..!!
//    mutating func incrementarMaxSpeed() {
//        maxSpeed += 10
//    }
}

var carA = Car(maxSpeed: 200, brand: "VW")
carA.brand = "Audi"

class Person {
    let name: String
    let lastName: String
    let nId: String
    
    init(name: String, lastName: String, nId: String) {
        self.name = name
        self.lastName = lastName
        self.nId = nId
    }
}

let personA = Person(name: "Jonathan", lastName: "Parra", nId: "1720446218")

// Herencia

protocol Friendable {
    var friend: Person? { get set }
    func addFriend(friend: Person)
}

class Employee: Person, Friendable {
    
    let jobTitle: String
    var salary: Double?
    var friend: Person?
    
    
    init(
        name: String,
        lastName: String,
        nId: String,
        jobTitle: String
    ) {
        self.jobTitle = jobTitle
        super.init(name: name, lastName: lastName, nId: nId)
    }
    
    func addFriend(friend: Person) {
        self.friend = friend
    }
    
    func singleFunction(var1: Int) {
        
    }
    
    func singleFunction(var1: String) {
        
    }
    
//    override func doSomething () {
//        print("I'm an employed")
//    }
}

// private -> solo lo que esta dentro de la clase
// fileprivate -> varias clases dentro
// Polimorfismo -> cambiar el comportamiento

func polimorphic(person: Person) {
    print(person.name)
}

let employee = Employee(name: "Javier", lastName: "Socasi", nId: "1710995091", jobTitle: "Software y Computación")

var myOptional: Int?

print(myOptional)

myOptional = 10

print(myOptional)

func somethingCool (number: Int) {
    print("number is: \(number)")
}

somethingCool(number: myOptional ?? 0)

func otherSomething(number: Int?) {
    if let noNumber = number, noNumber % 2 == 0 {
        print("number is \(noNumber)")
        noNumber
    }
}

myOptional = nil
otherSomething(number: myOptional)

func moreOptionals(number: Int?) {
    guard let noNumber = number else {
        print("NUUULLL...!!!")
        return
    }
    
    print(noNumber)
}

moreOptionals(number: myOptional)

myOptional = 20

print(myOptional)

print(myOptional!)

myOptional = nil

//print(myOptional!)


