
import UIKit

var str = "Hello, playground"

for i in stride(from: 1, to: 10, by: 2) {
    print("Hello: \(i)")
}

for _ in 1...10 {
    print("PUTITO")
}

var numeros = [1,2,3,4,5]

var cualquierVerga: [Any]

let eventNumbers = numeros.filter { number in
    number % 2 == 0
}

let even = numeros
    .filter{ $0 % 2 == 0 }
    .map{ $0 * $0 }
    .map{ Double($0) * 0.15 }

let dictionary = [
    "name": "Ronald",
    "lastname": "Cargua",
    "age": "23"
]

dictionary["name"]


let temperature = 10

switch temperature {
case 1...8 where temperature % 2 == 0:
    print("It's cold! ")
case 9...12:
    print("Not too cold")
case 3...17:
    print("Just about right")
default:
    print("Ronald gey")
}
func myFunction(){
    print("Puto el que lo lea")
}
myFunction()

func square(number: Int) -> Int {
    return number * number
}
square(number: 10)

func message(men1: String, men2: String) {
    print("Hola mekin \(men1) \(men2)")
}
message(men1: "verga", men2: "XD")

func square2(_ number: Int, _ number2: Int) -> Int {
    return number * number * number2
}
square2(4, 5)

if temperature > 20, temperature % 2 == 0 {
    print("if")
} else {
    print("else")
}

if temperature > 20, temperature % 2 == 0 {
    print("if")
}
if temperature < 15 {
    print("dentro de 15")
}
if temperature < 11 {
    print("dentro de 10")
}

func sSum(n1: Int, n2: Int, n3: Int = 0) -> Int {
    return n1 + n2 + n3
}
sSum(n1: 1, n2: 2)
sSum(n1: 3, n2: 6, n3: 2)

func mult(n1: Int, n2: Int) -> Int {
    return n1 * n2
}

func takes(n1: Int, n2: Int) -> Int {
    return n1 - n2
}

func opp(n1: Int, n2: Int, operation: ((Int, Int) -> Int)) -> Int {
    return operation(n1, n2)
}

opp(n1: 4, n2: 5, operation: takes(n1:n2:))

opp(n1: 2, n2: 3, operation: mult(n1:n2:))

func specialSum<T:Numeric>(n1: T, n2: T) -> T {
    return n1 + n2
}

let r1 = specialSum(n1: 4.3, n2: 5.9)
r1

let r2 = specialSum(n1: 3, n2: 1)
r2

let r3 = specialSum(n1: 2.5, n2: 4)
r3

func recursive(a: Int, sum: Int) -> Int {
    if (a == 0) {
        return sum
    } else {
        return recursive(a: a - 1, sum: sum + a)
    }
}

recursive(a: 10, sum: 0)

let coord2D = (4,5)
let coord3D = (4.1, 6, 8.43)

func badFunction(sideSize: Int) -> (p: Int,a: Int) {
    return (sideSize * 4, sideSize * sideSize)
}

badFunction(sideSize: 5).p

// coord2D.x
// coord3D.z


